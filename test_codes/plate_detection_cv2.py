import cv2
import sys
import numpy as np


img_path = sys.argv[1]
# img_path = '/mnt/data/loingo.data/alpr/images_set/bien-so-xe-may/5-xe_arca.jpg'
ratio = 3


def nothing(para_1, **para_2):
    pass


def canny_thresholding(img, lower_threshold):
    detected_edges = cv2.Canny(img, lower_threshold, lower_threshold*ratio)
    return detected_edges


def HoughLines_transform(edges_img):
    empty_img = np.zeros(edges_img.shape)
    lines = cv2.HoughLines(edges_img, 1, np.pi / 180, 50)
    if lines is not None:
        for line in lines:
            rho, theta = line[0]
            a = np.cos(theta)
            b = np.sin(theta)
            x0 = a*rho
            y0 = b*rho
            x1 = int(x0 + 1000*(-b))
            y1 = int(y0 + 1000*(a))
            x2 = int(x0 - 1000*(-b))
            y2 = int(y0 - 1000*(a))
            cv2.line(empty_img,(x1,y1),(x2,y2),(255,255,255))
    return empty_img


def to_rectangle(contour):
    # ref: https://www.pyimagesearch.com/2016/02/08/opencv-shape-detection/
    peri = cv2.arcLength(contour, True)
    approx = cv2.approxPolyDP(contour, 0.04 * peri, True)
    if len(approx) > 3:
        return cv2.boundingRect(approx)
    return None


def main():
    img = cv2.imread(img_path)
    kernel = np.ones((3,3))
    gray_img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    denoised_img = cv2.fastNlMeansDenoising(gray_img)
    # Code for tuning canny threshold and display
    # window_name = 'opencv detector'
    # cv2.namedWindow(window_name, cv2.WINDOW_AUTOSIZE)
    # cv2.resizeWindow(window_name, (480, 720))
    # cv2.createTrackbar('Thresholding', window_name, 0, 500, nothing)
    # while(1):
    #     lower_threshold = cv2.getTrackbarPos('Thresholding', window_name)
    #     detected_edges = canny_thresholding(denoised_img, lower_threshold)
    #     Hough_lines = HoughLines_transform(detected_edges)
    #     # detected_edges = cv2.morphologyEx(detected_edges, cv2.MORPH_CLOSE, kernel)
    #     # im2, contours, hierarchy = cv2.findContours(detected_edges, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    #     # contours_img = np.zeros(detected_edges.shape)
    #     # # contours_img = cv2.drawContours(contours_img, contours, -1, (255,255,255), 1)
    #     # for contour in contours:
    #     #     shape = to_rectangle(contour)
    #     #     if shape is not None:
    #     #         x,y,w,h = shape
    #     #         cv2.rectangle(contours_img,(x,y),(x+w,y+h),(255,255,255),1)
    #     display_img = np.concatenate((detected_edges, Hough_lines), axis=1)
    #     cv2.imshow(window_name, display_img)
    #     if cv2.waitKey(1) == 113:
    #         break
    threshold = 300
    detected_edges = canny_thresholding(denoised_img, threshold)
    morph = cv2.morphologyEx(detected_edges, cv2.MORPH_CLOSE, kernel)
    im2, contours, hierarchy = cv2.findContours(morph, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    contours_img = np.zeros(detected_edges.shape)
    contours_img = cv2.drawContours(contours_img, contours, -1, (255,255,255), 1)
    for contour in contours:
        shape = to_rectangle(contour)
        if shape is not None:
            x,y,w,h = shape
            cv2.rectangle(contours_img,(x,y),(x+w,y+h),(255,255,255),1)
    # Hough_lines = HoughLines_transform(detected_edges)
    display_img = np.concatenate((gray_img, detected_edges, contours_img), axis=0)
    cv2.imwrite('back_canny_' + str(threshold) + '_contours.jpg', display_img)


main()

