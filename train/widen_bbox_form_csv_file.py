import sys
import argparse


def main(args):
    widen_ratio = args.widen_ratio
    input_file = args.input
    output_file = args.output

    with open(input_file, 'r') as f:
            input_lines = f.readlines()


    output_lines = []
    for input_line in input_lines[1:]:
            label,filename,height,width,xmax,xmin,ymax,ymin = input_line.rstrip().split(',')
            widen_pixels = int(widen_ratio*int(height))

            new_xmax = int(xmax) + widen_pixels
            new_xmin = int(xmin) - widen_pixels
            new_ymax = int(ymax) + widen_pixels
            new_ymin = int(ymin) - widen_pixels
            new_height = new_ymax - new_ymin
            new_width = new_xmax - new_xmin

            output_lines.append(','.join([label,
                                         filename,
                                         str(new_height),
                                         str(new_width),
                                         str(new_xmax),
                                         str(new_xmin),
                                         str(new_ymax),
                                         str(new_ymin)]))


    with open(output_file, 'w') as f:
            f.write(input_lines[0])
            for output_line in output_lines:
                    f.write(output_line+'\n')


def parse_arguments(argv):
    parser = argparse.ArgumentParser('')
    parser.add_argument('-i', '--input', help='input file path')
    parser.add_argument('-o', '--output', help='output file path')
    parser.add_argument('-wr', '--widen_ratio', help='widen ratio of bouding box', type=float)
    return parser.parse_args()


if __name__ == '__main__':
    main(parse_arguments(sys.argv[1:]))
