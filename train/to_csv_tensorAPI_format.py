# text file input format
# image_name,license_text,bb_tlx,bb_tly,bb_brx,bb_bry,poly_tlx,poly_tly,poly_trx,poly_try,poly_blx,poly_bly,poly_brx,poly_bry
# text file output format
# class,filename,height,width,xmax,xmin,ymax,ymin

import sys
import os
import random
import argparse


def write_to_file(lines, save_path):
    out_f = open(save_path, 'w')
    out_f.write('class,filename,height,width,xmax,xmin,ymax,ymin\n')
    class_name = 'license_plate'
    for line in lines:
        file_name,_,bb_tlx,bb_tly,bb_brx,bb_bry, = line.rstrip().split(',')[:6]
        bb_tlx,bb_tly,bb_brx,bb_bry = [int(x) for x in [bb_tlx,bb_tly,bb_brx,bb_bry]]
        height = bb_bry - bb_tly
        width = bb_brx - bb_tlx
        xmax = bb_brx
        xmin = bb_tlx
        ymax = bb_bry
        ymin = bb_tly
        height,width,xmax,xmin,ymax,ymin = [str(x) for x in [height,width,xmax,xmin,ymax,ymin]]
        ouput_line = ','.join([class_name,file_name,height,width,xmax,xmin,ymax,ymin])
        out_f.write(ouput_line + '\n')
    print('file converted saved at', save_path)
    out_f.close()


def main(args):
    input_file = args.input
    save_path = args.output
    original_file_name = os.path.splitext(os.path.basename(input_file))[0]
    with open(input_file, 'r') as inp_f:
        input_lines = inp_f.readlines()

    input_lines = input_lines[1:]
    random.shuffle(input_lines)

    if args.split:
        split_idx = int(0.7*len(input_lines))
        train_data = input_lines[:split_idx]
        validate_data = input_lines[split_idx:]
        original_file_name = os.path.splitext(os.path.basename(input_file))[0]
        write_to_file(train_data, os.path.join(save_path, original_file_name + '_train'))
        write_to_file(validate_data, os.path.join(save_path, original_file_name + '_validate'))
    else:
        write_to_file(input_lines, save_path)


def parse_arguments(argv):
    parser = argparse.ArgumentParser('')
    parser.add_argument('-i', '--input', help='input csv from generate_scripts file')
    parser.add_argument('-o', '--output', help='output directory for saving')
    parser.add_argument('-s', '--split', help='do split to train, validation set', action='store_true')
    return parser.parse_args()


if __name__ == '__main__':
    main(parse_arguments(sys.argv[1:]))
