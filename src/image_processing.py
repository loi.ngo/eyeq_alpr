import cv2
import numpy as np
import random
from config import Config
from utils import random_coin


class ImageProcessing:
    def equalize_histogram(self, source_image):
        # https://stackoverflow.com/questions/15007304/histogram-equalization-not-working-on-color-image-opencv
        YCrCb_image = cv2.cvtColor(source_image, cv2.COLOR_BGR2YCrCb)
        channels = cv2.split(YCrCb_image)
        channels[0] = cv2.equalizeHist(channels[0])
        YCrCb_image = cv2.merge(channels)
        bgr_image = cv2.cvtColor(YCrCb_image, cv2.COLOR_YCrCb2BGR)
        return bgr_image

    def add_random_noise(self, source_image, noise_rate=Config.RANDOM_NOISE_RATE):
        h, w, c = source_image.shape
        noise = np.random.randn(h, w, c) * noise_rate
        noised_image = source_image + noise.astype(np.uint8)
        return noised_image

    def motion_blur(self, source_image, kernel_size=5):
        kernel_motion_blur = np.zeros((kernel_size, kernel_size))
        kernel_motion_blur[int((kernel_size-1)/2), :] = np.ones(kernel_size)
        kernel_motion_blur = kernel_motion_blur / kernel_size

        # applying the kernel to the input image
        output = cv2.filter2D(source_image, -1, kernel_motion_blur)
        return output

    def random_morphology(self, source_image, kernel_size=5):
        kernel_morphology = np.ones((kernel_size, kernel_size))
        if random.random() > 0.5:
            output = cv2.erode(source_image, kernel_morphology)
        else:
            output = cv2.dilate(source_image, kernel_morphology)
        return output

    def add_shadow(self, source_image):
        h,w = source_image.shape[:2]
        # get two random point that form a split line
        triangle_vertice = self.get_random_triangle_vertice((h,w))
        # random two points to decrease lighting
        # illumination_decrease = random.randint(Config.ILLUMINATION_DECREASE_RANGE)
        # now decrease the illumination values
        ## Conversion to HLS
        image_HLS = cv2.cvtColor(source_image, cv2.COLOR_RGB2HLS)
        mask = np.zeros_like(source_image)
        imshape = source_image.shape
        ## adding all shadow polygons on empty mask, single 255 denotes only red channel
        cv2.fillPoly(mask, triangle_vertice, 255) 
        ## if red channel is hot, image's "Lightness" channel's brightness is lowered
        image_HLS[:,:,1][mask[:,:,0]==255] = image_HLS[:,:,1][mask[:,:,0]==255]*0.5
        ## Conversion to RGB
        image_RGB = cv2.cvtColor(image_HLS,cv2.COLOR_HLS2RGB)
        return image_RGB

    def random_transformation(self, source_image):
        output_image = source_image
        if random_coin():
            output_image = self.add_random_noise(output_image)
        if random_coin():
            output_image = self.motion_blur(output_image)
        if random_coin():
            output_image = self.random_morphology(output_image)
        if random_coin():
            output_image = self.add_shadow(output_image)
        return output_image

    def get_random_triangle_vertice(self, image_shape):
        h,w = image_shape
        point_1 = (0, 0) if random_coin() else (h,w)
        # get top left point
        point_2 = (random.randint(0, h), 0) if random_coin() else (0, random.randint(0, w))
        # get random bottom right point
        point_3 = (random.randint(0, h), w) if random_coin else (h, random.randint(0, w))
        return np.array([point_1, point_2, point_3], dtype=np.int32)
