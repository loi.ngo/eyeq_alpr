### ref: https://blog.metaflow.fr/tensorflow-how-to-freeze-a-model-and-serve-it-with-a-python-api-d4f3596b3adc


import tensorflow as tf
import numpy as np
import glob


class PlateDetector():
    def __init__(self, frozen_graph_filename, threshold=0.7):
        # load graph file from disk and parse it to retrieve
        with tf.gfile.GFile(frozen_graph_filename, 'rb') as f:
            graph_def = tf.GraphDef()
            graph_def.ParseFromString(f.read())
        # import the graph_def to new graph and return 
        with tf.Graph().as_default() as graph:
            tf.import_graph_def(graph_def, name='prefix')

        self.threshold = threshold
        self.graph = graph
        self.image_tensor = self.graph.get_tensor_by_name('prefix/image_tensor:0')
        self.detection_boxes = self.graph.get_tensor_by_name('prefix/detection_boxes:0')
        self.detection_scores = self.graph.get_tensor_by_name('prefix/detection_scores:0')
        self.session = tf.Session(graph=graph)
        self.detection_classes = self.graph.get_tensor_by_name('prefix/detection_classes:0')

    def detect_plates(self, img_tensor):
        result_boxes = []
        if img_tensor.ndim < 4:
            img_tensor = np.expand_dims(img_tensor, axis=0)
        boxes, scores, classes = self.session.run([self.detection_boxes, self.detection_scores, self.detection_classes], feed_dict={self.image_tensor: img_tensor})
        # print(boxes.shape, scores.shape)
        # print(img_tensor.shape)
        nrof_imgs = boxes.shape[0]
        for img_idx in range(nrof_imgs):
            this_img_bboxes = np.array([])
            this_img_scores = scores[img_idx]
            this_img_boxes = boxes[img_idx]
            if this_img_scores.any():
                # print(this_img_scores.sum())
                pass_threshold_idx = this_img_scores > self.threshold
                this_img_bboxes = this_img_boxes[pass_threshold_idx, :]
                pass_scores = this_img_scores[pass_threshold_idx]
                # print(pass_scores.shape, this_img_bboxes.shape)
                # print(pass_scores)
                h_w = img_tensor[img_idx].shape[:2]
                this_img_bboxes = self.unnormalize_bbox(h_w, this_img_bboxes)
            result_boxes.append(this_img_bboxes)
        return np.array(result_boxes)

    def unnormalize_bbox(self, img_h_w, normalized_bboxes):
        h,w = img_h_w
        mutiply_matrix = np.array(([h, w, h, w]))
        # element wise multiply
        unnormalized = normalized_bboxes * mutiply_matrix
        # for idx in range(normalized_bboxes.shape[0]):
        #     tly = int(normalized_bbox[0]*h)
        #     tlx = int(normalized_bbox[1]*w)
        #     bry = int(normalized_bbox[2]*h)
        #     brx = int(normalized_bbox[3]*w)
        # idk why the bboxes array is contructed as [tly, tlx, bry, brx]
        # swap back to xy format
        unnormalized = unnormalized[:, [1,0,3,2]]
        return unnormalized.astype(int)
