import sys
import argparse
import os
import glob
import cv2
from plate_detector import PlateDetector
import shutil


def detect_and_write(frozen_graph_filename, score_threshold, input_dir, save_dir):
    detector = PlateDetector(frozen_graph_filename, score_threshold)
    img_paths = glob.glob(os.path.join(input_dir, '*.jpg'))
    img_paths += glob.glob(os.path.join(input_dir, '*.png'))
    for img_path in img_paths:
        img = cv2.imread(img_path)
        # rgb_img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
        bboxes = detector.detect_plates(img)
        bboxes = bboxes.squeeze(axis=0)
        for bbox in bboxes:
            img = cv2.rectangle(img, (bbox[0], bbox[1]), (bbox[2], bbox[3]), (0,255,0), 2)
        img_name = os.path.basename(img_path)
        save_path = os.path.join(save_dir, img_name)
        cv2.imwrite(save_path, img)
        print(img_name, bboxes)


def main(args):
    if os.path.exists(args.save_dir):
        shutil.rmtree(args.save_dir)
    os.mkdir(args.save_dir)
    detect_and_write(args.frozen_graph_filename, args.score_threshold, args.input_dir, args.save_dir)


def parse_arguments(argv):
    parser = argparse.ArgumentParser('')
    parser.add_argument('-id' ,'--input_dir', help='directory contain all jpg test images')
    parser.add_argument('-sd', '--save_dir', help='directory to save all output images')
    parser.add_argument('-fpf', '--frozen_graph_filename', help='path to pb model file')
    parser.add_argument('-st', '--score_threshold', help='threshold for detection', default=0.8, type=float)
    return parser.parse_args()


if __name__ == '__main__':
    main(parse_arguments(sys.argv[1:]))
