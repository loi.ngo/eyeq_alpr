import os
import math


current_dir = os.path.dirname(os.path.abspath(__file__))
ROOT = os.path.abspath(os.path.join(current_dir, os.path.pardir))
DATA_DIR = os.path.join('../data')


class Config:
    PLATE_FRONT_FILE = os.path.join(DATA_DIR, 'Soxe2banh.TTF')
    PLATE_TEMPLATE_IMAGE = os.path.join(DATA_DIR, 'plate_template.jpg')
    
    PLATE_CHARACTER_SET = 'QWERTYUIOPASDFGHJKLZXCVBNM'
    PLATE_NUMBER_SET = '1234567890'

    # PLATE_CHARACTER_CORLOR = (0, 0, 0)
    # PLATE_BACKGROUND_COLOR = (255, 255, 255)
    # 28/09/2018 change from fixed value color code to random in a range
    PLATE_CHARACTER_CORLOR_RANGE = (0, 50)
    PLATE_BACKGROUND_COLOR_RANGE = (200, 255)
    # because when random each channel, color may come out not black or gray
    # so now we only pickl a value and variances color in that += PLATE_IN_COLOR_VARIANCE_RANGE
    PLATE_IN_COLOR_VARIANCE_RANGE = 10
    PLATE_BACKGROUND_COLOR_CODE = 255

    MIN_PLATE_WIDTH = 20
    PLATE_RATIO = 1.36
    # PLATE_DEFAULT_HEIGHT = 40 # 350
    # PLATE_DEFAULT_WIDTH = 55 # 475

    # PLATE_FRONT_SIZE = int(PLATE_DEFAULT_HEIGHT*0.5)
    # DASHLINE_SIZE = (int(PLATE_DEFAULT_HEIGHT*0.085), int(PLATE_DEFAULT_HEIGHT*0.06))
    # DOT_SIZE = (int(PLATE_DEFAULT_HEIGHT*0.06), int(PLATE_DEFAULT_HEIGHT*0.06))
    # CHARACTER_OFFSET = int(PLATE_DEFAULT_HEIGHT*0.05)
    # INLINE_SPACE_SIZE = int(PLATE_DEFAULT_HEIGHT*0.04)
    # DOT_SPACE_SIZE = int(PLATE_DEFAULT_HEIGHT*0.015)

    PLATE_FRONT_SIZE_RATE = 0.5
    CHARACTER_OFFSET_RATE = 0.05
    DASHLINE_SIZE_RATE = (0.085, 0.06)
    INLINE_SPACE_SIZE_RATE = 0.04
    DOT_SPACE_SIZE_RATE = 0.015
    DOT_CHARACTER_SIZE_RATE = (0.06, 0.06)

    # Config for plate warpping
    PLATE_HEIGHT_PERCENT_WRT_BACKGROUND = 0.7
    MAX_TILT_ANGLE = math.pi/18
    MAX_STRECTCHING_PERCENT = 0.2

    RANDOM_NOISE_RATE = 0.5
    BACKGROUND_NOISE_RATIO = 0.1

    # Config for image processing values
    ILLUMINATION_DECREASE_RANGE = 50