import os
import sys
import glob
import time
import random
import shutil
import argparse
import numpy as np
import cv2
from config import Config
from plate_generator import PlateGenerator
from plate_warpping import PlateWarpping
from image_processing import ImageProcessing
from utils import create_new_folder, flat_points_list_to_string_list, random_coin


class PlateGetter():
    def __init__(self, input_path=None):
        self.generating_mode = False
        if input_path is None:
            self.generator = PlateGenerator()
            self.generating_mode = True
        # augmentation mode
        else:
            glob_path = os.path.join(input_path, '*.jpg')
            self.input_plate_paths = glob.glob(glob_path)

    def get_random_plate(self, plate_width):
        if self.generating_mode:
            plate_img, full_license_text = self.generator.generate_plate(plate_width)
        else:
            plate_path = random.choice(self.input_plate_paths)
            plate_img = cv2.imread(plate_path)
            plate_img = cv2.resize(plate_img, (int(plate_width/Config.PLATE_RATIO),plate_width))
            full_license_text = ''
        
        return plate_img, full_license_text


def generate_plates_only(nrof_plates, save_dir):
    generator = PlateGenerator()
    warpper = PlateWarpping()
    for i in range(nrof_plates):
        plate_width = random.randint(100, 1000)
        noise_rate = random.random()
        plate_img, full_license_text = generator.generate_plate(plate_width)
        plate_img = warpper.transform_plate(plate_img)
        print(full_license_text)
        save_name = os.path.join(save_dir, full_license_text + '.jpg')
        cv2.imwrite(save_name, plate_img)


def generate_plate_w_background(nrof_plates, save_dir, backgrounds, log_file_path, plates_input_path=None):
    warpper = PlateWarpping()
    processor = ImageProcessing()
    log_file = open(log_file_path, 'w')
    log_file.write('image_name,license_text,bb_tlx,bb_tly,bb_brx,bb_bry,poly_tlx,poly_tly,poly_trx,poly_try,poly_blx,poly_bly,poly_brx,poly_bry\n')
    # read all background path
    backgrounds_paths = glob.glob(os.path.join(backgrounds, '*.jpg'))
    # hard code this, gonna fix later
    plate_getter = PlateGetter(plates_input_path)
    fill_num = len(str(nrof_plates))
    i = 1
    while i < nrof_plates + 1:
        background_path = random.choice(backgrounds_paths)
        background_img = cv2.imread(background_path)
        max_plate_width = warpper.get_max_plate_width(background_img.shape[:2])
        # incase max_plate_width < Config.MIN_PLATE_WIDTH
        try:
            plate_width = random.randint(Config.MIN_PLATE_WIDTH, max_plate_width)
            plate_img, full_license_text = plate_getter.get_random_plate(plate_width)
            final_img, bouding_box, polygon = warpper.blend_to_background(plate_img, background_img)
        except ValueError:
           continue
        # add noise to final_img
        final_img = processor.equalize_histogram(final_img)
        final_img = processor.add_random_noise(final_img)
        if random_coin(0.7):
            final_img = processor.motion_blur(final_img)
        # saving and write logs
        img_name = str(i).zfill(fill_num) + '.jpg'
        img_save_path = os.path.join(save_dir, img_name)
        bouding_box_list = flat_points_list_to_string_list(bouding_box)
        polygon_list = flat_points_list_to_string_list(polygon)

        log_line_list = [img_name, ''] + bouding_box_list + polygon_list
        log_line = ','.join(log_line_list)
        log_file.write(log_line + '\n')
        cv2.imwrite(img_save_path, final_img)
        ### print processing bar:
        # os.system('clear')
        complete_percent = i*100/nrof_plates
        processing_bar = '='*int(complete_percent*50/100)
        print('{}> {}%'.format(processing_bar, int(complete_percent)), end='\r')
        i += 1
    log_file.close()


def main(args):
    start = time.time()
    random.seed(args.random_seed)
    if args.delete:
        if os.path.exists(args.log_file):
            os.remove(args.log_file)
        if os.path.exists(args.save_dir):
            shutil.rmtree(args.save_dir)
    # checking input availability
    if not os.path.exists(args.backgrounds):
        print('Please check backgrounds path availability')
    if not os.path.exists(args.save_dir):
        create_new_folder(args.save_dir)
    open(args.log_file, 'a').close()

    if args.backgrounds == None:
        generate_plates_only(args.nrof_plates, args.save_dir)
    else:
        generate_plate_w_background(args.nrof_plates, args.save_dir, args.backgrounds, args.log_file, args.plates_image_dir)

    print('\ntotal time ', time.time() - start)
    # start = time.time()
    # background = np.array(Image.open('/home/loingo/git/eyeq_alpr/data/road1.jpg'))
    # final_img = combine_plate_background(plate_img, background)
    # print('plate:', full_license_text)
    # print('run time', time.time() - start)
    # plt.imshow(final_img)
    # plt.show()


def parse_arguments(argv):
    parser = argparse.ArgumentParser(description='parser for alpr data generator')
    parser.add_argument('-np', '--nrof_plates', help='number of license plate', type=int)
    parser.add_argument('-bg', '--backgrounds', help='directory contain all backgrounds images', default=None)
    parser.add_argument('-sd', '--save_dir', help='save directory for plates')
    parser.add_argument('-lf', '--log_file', help='log file directory')
    parser.add_argument('-d', '--delete', help='delete input directory', action='store_true')
    parser.add_argument('-rs', '--random_seed', help='set random seed', type=int, default=None)
    parser.add_argument('-pid', '--plates_image_dir', help='directory that contain cropped plates', default=None)
    return parser.parse_args()


if __name__ == '__main__':
    main(parse_arguments(sys.argv[1:]))
