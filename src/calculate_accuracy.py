import sys
import pickle
import argparse
from utils import cacl_iou, load_csv_groundtruth


def main(args):
    path_bbox = load_csv_groundtruth(args.ground_truth_file)
    predicted_dict = pickle.load(open(args.predicted_dict_file, 'rb'))

    base_iou_threshold = 0.1
    for step in range(3, 9):
        iou_threshold = round(base_iou_threshold*step, 1)
        acc_count = 0
        for img_name, bbox in path_bbox:
            if img_name in predicted_dict:
                predict_bboxes = predicted_dict[img_name]
                for predict_bbox in predict_bboxes:
                    if cacl_iou(bbox, predict_bbox) > iou_threshold:
                        acc_count += 1

        print('iou_threshold={}, top1_accuracy:{}'.format(iou_threshold, round(acc_count/len(path_bbox), 4)))


def parse_arguments(argv):
    parser = argparse.ArgumentParser('')
    parser.add_argument('-gtf', '--ground_truth_file', default='/mnt/data/loingo.data/alpr/training_plates_100000/config_data/plates_100000_validate.csv_full')
    parser.add_argument('-pdf', '--predicted_dict_file')
    return parser.parse_args()


main(parse_arguments(sys.argv[1:]))
