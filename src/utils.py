import os
import shutil
import random


def create_new_folder(folder_path):
    if os.path.exists(folder_path):
        raise Exception(folder_path, ' existed')
    os.mkdir(folder_path)


def flat_points_list_to_string_list(points_list):
    string_list = []
    for x,y in points_list:
        string_list.append(str(x))
        string_list.append(str(y))
    return string_list


def load_csv_groundtruth(csv_file):
    path_bbox = []
    with open(csv_file, 'r') as f:
        lines = f.readlines()
    for line in lines[1:]:
        _,img_name,_,_,xmax,xmin,ymax,ymin = line.split(',')
        path_bbox.append((img_name,[int(xmin),int(ymin),int(xmax),int(ymax)]))
    return path_bbox


def cacl_iou(bb1, bb2):
    """
    Check overlap between 2 bounding boxes
    Return: float in [0,1]
    >>> check_overlap([1, 1, 3, 4], [1, 2, 3, 5])
    0.5
    """
    assert bb1[0] < bb1[2]
    assert bb1[1] < bb1[3]
    assert bb2[0] < bb2[2]
    assert bb2[1] < bb2[3]

    # determine the coordinates of the intersection rectangle
    x_left = max(bb1[0], bb2[0])
    y_top = max(bb1[1], bb2[1])
    x_right = min(bb1[2], bb2[2])
    y_bottom = min(bb1[3], bb2[3])

    if x_right < x_left or y_bottom < y_top:
        return 0.0

    # The intersection of two axis-aligned bounding boxes is always an
    # axis-aligned bounding box
    intersection_area = (x_right - x_left) * (y_bottom - y_top)

    # compute the area of both AABBs
    bb1_area = (bb1[2] - bb1[0]) * (bb1[3] - bb1[1])
    bb2_area = (bb2[2] - bb2[0]) * (bb2[3] - bb2[1])

    # compute the intersection over union by taking the intersection
    # area and dividing it by the sum of prediction + ground-truth
    # areas - the interesection area
    iou = intersection_area / float(bb1_area + bb2_area - intersection_area)
    assert iou >= 0.0
    assert iou <= 1.0
    return iou

def random_coin(threshold=0.5):
    return random.random() > threshold
