import cv2
import sys
import argparse
from plate_detector import PlateDetector 
import time

def main(args):
    detector_path = args.frozen_graph_file
    input_video_path = args.input_video
    detector = PlateDetector(detector_path)
    cap = cv2.VideoCapture(input_video_path)

    if args.show:
        cv2.namedWindow('vietnet', cv2.WINDOW_NORMAL)
        cv2.resizeWindow('vietnet', 720, 480)

    if args.write is not None:
        _, test_frame = cap.read()
        while test_frame is None:
            _, test_frame = cap.read()
        fh, fw = test_frame.shape[:2]
        fourcc = cv2.VideoWriter_fourcc(*'MJPG')
        out = cv2.VideoWriter(args.write, fourcc, 20.0, (fw,fh))
    frame_count = 0
    total_time = 0
    ret = True
    while ret:
        ret, frame = cap.read()
        if frame is None:
            continue
        start = time.time()
        bboxes = detector.detect_plates(frame)
        total_time += time.time() - start
        bboxes = bboxes.squeeze(axis=0)
        for bbox in bboxes:
            frame = cv2.rectangle(frame, (bbox[0], bbox[1]), (bbox[2], bbox[3]), (0,255,0), 2)
        if args.show:
            cv2.imshow('vietnet', frame)
        if args.write is not None:
            out.write(frame)
        if cv2.waitKey(1) == ord('q'):
            break
        frame_count += 1
        print('run time per frame', total_time/frame_count)


def parse_arguments(argv):
    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--input_video')
    parser.add_argument('-fgf', '--frozen_graph_file')
    parser.add_argument('-s', '--show', action='store_false')
    parser.add_argument('-w', '--write', help='input path to write video', default=None)
    return parser.parse_args()


if __name__ == '__main__':
    main(parse_arguments(sys.argv[1:]))