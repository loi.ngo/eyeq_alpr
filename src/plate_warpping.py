import cv2
import math
import random
import numpy as np
from config import Config

class PlateWarpping:
    def get_bouding_box(self, points):
        # input format [(tl_x, tl_y), (tr_x, tr_y), (bl_x, bl_y), (br_x, br_y)])
        [(tl_x, tl_y), (tr_x, tr_y), (bl_x, bl_y), (br_x, br_y)] = points
        bbox_tl_x = min(tl_x, bl_x)
        bbox_tl_y = min(tl_y, tr_y) 
        bbox_br_x = max(tr_x, br_x)
        bbox_br_y = max(bl_y, br_y)
        return (bbox_tl_x, bbox_tl_y), (bbox_br_x, bbox_br_y)

    def get_max_plate_width(self, background_image_size):
        # solve equation
        # bg_h - 2(plate_h + 2*plate_w*cos(max_tilt_angle)) = 0
        # bg_w - 2(plate_w + 2*plate_h*cos(max_tilt_angle)) = 0
        # in which plate_h = plate_w/Config.PLATE_RATIO(= 1.36)
        bg_h, bg_w = background_image_size
        cos_value = math.cos(Config.MAX_TILT_ANGLE)
        plate_w_wst_bgh = bg_h/(2*(1/Config.PLATE_RATIO + 2*cos_value))
        plate_w_wst_bgw = bg_w/(2*(1 + 2/Config.PLATE_RATIO*cos_value))
        # 10 is the offset
        max_plate_w = int(min(plate_w_wst_bgh, plate_w_wst_bgw)) - 10
        # plate_h = int(max_plate_w/Config.PLATE_RATIO)
        return max_plate_w

    def get_tilt_angle(self):
        sign = 1
        if random.random() > 0.5:
            sign = -1
        return random.random() * Config.MAX_TILT_ANGLE * sign

    def random_destination_polygon_points(self, plate_h_w, background_image_size, background_margin=None):
        # limit the ROI to generate polygon points in it
        plate_h, plate_w = plate_h_w
        bg_h, bg_w = background_image_size
        # do Strectching
        plate_h += int(plate_h * random.random()*Config.MAX_STRECTCHING_PERCENT)
        plate_w += int(plate_w * random.random()*Config.MAX_STRECTCHING_PERCENT)

        # if input margin = None, we compute the safe margin
        if background_margin == None:
            background_margin_h = (plate_h + plate_w * math.cos(Config.MAX_TILT_ANGLE) * 2) / bg_h
            background_margin_w = (plate_w + plate_h * math.cos(Config.MAX_TILT_ANGLE) * 2) / bg_w
        else:
            background_margin_h = background_margin[0]
            background_margin_w = background_margin[1]
        h_margin = int(bg_h * background_margin_h)
        w_margin = int(bg_w * background_margin_w)

        # this might have bug that there is no region left after cut the margin
        tl_x = random.randint(0 + w_margin, bg_w - w_margin)
        tl_y = random.randint(0 + h_margin, bg_h - h_margin)

        # compute tr point base on the tilt angle and plate width
        tr_angle = self.get_tilt_angle()
        tr_x = tl_x + int(plate_w * math.cos(tr_angle))
        tr_y = tl_y + int(plate_h * math.sin(tr_angle))

        bl_angle = self.get_tilt_angle()
        bl_x = tl_x + int(plate_w * math.sin(bl_angle))
        bl_y = tl_y + int(plate_h * math.cos(bl_angle))

        br_angle = self.get_tilt_angle()
        br_x = tr_x + int(plate_w * math.sin(br_angle))
        br_y = tr_y + int(plate_h * math.cos(br_angle))

        return np.array([(tl_x, tl_y), (tr_x, tr_y), (bl_x, bl_y), (br_x, br_y)])

    def add_noise_from_background(self, plate_mask, background_image):
        # add noise from background to make plate look more real
        return cv2.addWeighted(background_image, 0.1, plate_mask, 1.0, 0)

    def overlay_plate(self, plate_mask, background_image, add_noise=True):
        plate_binary_mask = plate_mask.sum(axis=-1) != 255
        plate_binary_mask = np.stack([plate_binary_mask, plate_binary_mask, plate_binary_mask], axis=-1)
        if add_noise:
            plate_mask = self.add_noise_from_background(plate_mask, background_image)
        np.copyto(background_image, plate_mask, where=plate_binary_mask)
        return background_image

    def blend_to_background(self, plate_image, background_image):
        plate_h, plate_w = plate_image.shape[:2]
        source_pts = np.array([[0, 0],
                               [plate_w, 0],
                               [0, plate_h],
                               [plate_w, plate_h]])
        # random polygon points to warp plate
        # plate_h = int(bg_h * Config.PLATE_HEIGHT_PERCENT_WRT_BACKGROUND)
        # plate_w = int(plate_h * Config.PLATE_RATIO)
        dest_pts = self.random_destination_polygon_points((plate_h, plate_w), background_image.shape[:2])
        homo_matrix, _ = cv2.findHomography(source_pts, dest_pts)
        # set border to green for next overlay step
        warped_plate_mask = cv2.warpPerspective(plate_image,
                                                homo_matrix,
                                                (background_image.shape[1], background_image.shape[0]),
                                                borderValue=(0,255,0))

        final_img =self.overlay_plate(warped_plate_mask, background_image)

        bouding_box = self.get_bouding_box(dest_pts)
        return final_img, bouding_box, dest_pts

    def transform_plate(self, plate_image):
        # create empty background
        plate_h, plate_w, c = plate_image.shape
        empty_bg = np.zeros((plate_h*3, plate_w*3, c))
        dest_pts = self.random_destination_polygon_points((plate_h, plate_w), empty_bg, (0.2, 0.2))
        source_pts = np.array([[0, 0],
                               [plate_w, 0],
                               [0, plate_h],
                               [plate_w, plate_h]])
        homo_matrix, _ = cv2.findHomography(source_pts, dest_pts)
        warped_plate_mask = cv2.warpPerspective(plate_image,
                                                homo_matrix,
                                                (empty_bg.shape[1], empty_bg.shape[0]),
                                                borderValue=(0,255,0))
        tl, br = self.get_bouding_box(dest_pts)
        cropped_plate = warped_plate_mask[tl[1]:br[1], tl[0]:br[0], :]
        return cropped_plate

