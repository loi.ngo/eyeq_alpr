# import the necessary packages
import argparse
import cv2
import numpy as np
import os
import glob
import time
import sys
# initialize the list of reference points and boolean indicating
# whether cropping is being performed or not
refPt = []
cropping = False
image = None
 
def click_and_crop(event, x, y, flags, param):
    # grab references to the global variables
    global refPt, cropping

    if event == cv2.EVENT_LBUTTONDOWN:
        refPt.append((x, y))
        global image
        image = cv2.circle(image, (x, y), 3, (0,255,0), -1)
        cropping = True
        # print('down')

# construct the argument parser and parse the arguments

def order_points(pts):
    # initialzie a list of coordinates that will be ordered
    # such that the first entry in the list is the top-left,
    # the second entry is the top-right, the third is the
    # bottom-right, and the fourth is the bottom-left
    rect = np.zeros((4, 2), dtype = "float32")
 
    # the top-left point will have the smallest sum, whereas
    # the bottom-right point will have the largest sum
    s = pts.sum(axis = 1)
    rect[0] = pts[np.argmin(s)]
    rect[2] = pts[np.argmax(s)]
 
    # now, compute the difference between the points, the
    # top-right point will have the smallest difference,
    # whereas the bottom-left will have the largest difference
    diff = np.diff(pts, axis = 1)
    rect[1] = pts[np.argmin(diff)]
    rect[3] = pts[np.argmax(diff)]
 
    # return the ordered coordinates
    return rect


def four_point_transform(image, pts):
    # obtain a consistent order of the points and unpack them
    # individually
    rect = order_points(pts)
    (tl, tr, br, bl) = rect
 
    # compute the width of the new image, which will be the
    # maximum distance between bottom-right and bottom-left
    # x-coordiates or the top-right and top-left x-coordinates
    widthA = np.sqrt(((br[0] - bl[0]) ** 2) + ((br[1] - bl[1]) ** 2))
    widthB = np.sqrt(((tr[0] - tl[0]) ** 2) + ((tr[1] - tl[1]) ** 2))
    maxWidth = max(int(widthA), int(widthB))
 
    # compute the height of the new image, which will be the
    # maximum distance between the top-right and bottom-right
    # y-coordinates or the top-left and bottom-left y-coordinates
    heightA = np.sqrt(((tr[0] - br[0]) ** 2) + ((tr[1] - br[1]) ** 2))
    heightB = np.sqrt(((tl[0] - bl[0]) ** 2) + ((tl[1] - bl[1]) ** 2))
    maxHeight = max(int(heightA), int(heightB))
 
    # now that we have the dimensions of the new image, construct
    # the set of destination points to obtain a "birds eye view",
    # (i.e. top-down view) of the image, again specifying points
    # in the top-left, top-right, bottom-right, and bottom-left
    # order
    dst = np.array([
        [0, 0],
        [maxWidth - 1, 0],
        [maxWidth - 1, maxHeight - 1],
        [0, maxHeight - 1]], dtype="float32")
 
    # compute the perspective transform matrix and then apply it
    M = cv2.getPerspectiveTransform(rect, dst)
    warped = cv2.warpPerspective(image, M, (maxWidth, maxHeight))
 
    # return the warped image
    return warped

def run_process(frame, output_dir):
    global image
    global refPt
    image = frame
    clone = image.copy()
    while True:
        # display the image and wait for a keypress
        cv2.imshow("image", image)
        key = cv2.waitKey(1) & 0xFF

        # if the 'r' key is pressed, reset the cropping region
        if key == ord("r"):
            image = clone.copy()
            refPt = []

        if key == ord("w"):
            if len(refPt) == 4:
                warped = four_point_transform(clone, np.array(refPt, dtype=np.int32))
                write_path = os.path.join(output_dir, str(time.time()) + '.jpg')
                cv2.imwrite(write_path, warped)
                print('image is written at', write_path)
            else:
                print('Please pick exactly 4 points')
            image = clone.copy()
            refPt = []
        # if len(refPt) == 4:
        #     warped = four_point_transform(image, np.array(refPt, dtype=np.int32))
        #     refPt = []
        #     cv2.imshow("ROI", warped)
        #     if cv2.waitKey() == ord("s"):
        #         write_path = os.path.join(output_dir, str(time.time()) + '.jpg')
        #         cv2.imwrite(write_path, warped)
        #         cv2.destroyWindow('ROI')
        #     if key == ord("q"):
        #         cv2.destroyWindow('ROI')
        if key == ord("q"):
            break
        if key == ord("c"):
            sys.exit()
        # print(refPt)

ap = argparse.ArgumentParser()
ap.add_argument("-i", "--input", required=True, help="Path to the image")
ap.add_argument("-o", "--output", required=True, help="Path to the output save image")
args = vars(ap.parse_args())

# load the image, clone it, and setup the mouse callback function
cv2.namedWindow("image", cv2.WINDOW_NORMAL)
cv2.resizeWindow("image", 720, 480)
cv2.setMouseCallback("image", click_and_crop)

input_url = args["input"]
output_dir = args["output"]

if os.path.isdir(input_url):
    img_paths = glob.glob(os.path.join(input_url, '*.jpg'))
    for img_path in img_paths:
        img = cv2.imread(img_path)
        run_process(img, output_dir)
elif os.path.isfile(input_url):
    cap = cv2.VideoCapture(input_url)
    frame_num = 0
    while True:
        frame_num += 1
        ret, frame = cap.read()
        if frame_num % 10 == 0:
            run_process(frame, output_dir)
