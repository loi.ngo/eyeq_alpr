import sys
import random
from config import Config
from PIL import ImageFont, ImageDraw, Image
import cv2
import numpy as np


class PlateGenerator():
    def random_choices(self, seq, k):
        result = ''
        for i in range(k):
            result += random.choice(seq)
        return result

    def random_number_code(self):
        if random.random() > 0.5:
            return self.random_choices(Config.PLATE_NUMBER_SET, 5)
        return self.random_choices(Config.PLATE_NUMBER_SET, 4)

    def get_random_character_color(self):
        anchor_color_value = random.randint(*Config.PLATE_CHARACTER_CORLOR_RANGE)
        atual_color_range = (anchor_color_value - Config.PLATE_IN_COLOR_VARIANCE_RANGE,
                             anchor_color_value + Config.PLATE_IN_COLOR_VARIANCE_RANGE)
        return (random.randint(*atual_color_range),
                random.randint(*atual_color_range),
                random.randint(*atual_color_range))

    def get_random_background_color(self):
        anchor_color_value = random.randint(*Config.PLATE_BACKGROUND_COLOR_RANGE)
        atual_color_range = (anchor_color_value - Config.PLATE_IN_COLOR_VARIANCE_RANGE,
                             anchor_color_value + Config.PLATE_IN_COLOR_VARIANCE_RANGE)
        return (random.randint(*atual_color_range),
                random.randint(*atual_color_range),
                random.randint(*atual_color_range))

    def generate_plate(self, plate_width):
        plate_height = int(plate_width / Config.PLATE_RATIO)
        character_offset = int(plate_height * Config.CHARACTER_OFFSET_RATE)
        inline_space_size = int(plate_height * Config.INLINE_SPACE_SIZE_RATE)
        dashline_size = (int(plate_height * Config.DASHLINE_SIZE_RATE[0]), int(plate_height * Config.DASHLINE_SIZE_RATE[1]))
        dot_space_size = (int(plate_height * Config.DOT_SPACE_SIZE_RATE))
        dot_character_size = (int(plate_height * Config.DOT_CHARACTER_SIZE_RATE[0]), int(plate_height * Config.DOT_CHARACTER_SIZE_RATE[1]))
        # loading a plate template then size will lead to some scatter pixels, so here we draw the outline rectangle
        font = ImageFont.truetype(Config.PLATE_FRONT_FILE, int(plate_height*Config.PLATE_FRONT_SIZE_RATE))

        province_code = self.random_choices(Config.PLATE_NUMBER_SET, 2)
        area_code = self.random_choices(Config.PLATE_CHARACTER_SET, 1) + self.random_choices(Config.PLATE_NUMBER_SET, 1)
        number_code = self.random_number_code()

        y_space = int((plate_height - font.getsize(province_code)[1]*2)/3)
        top_text_y_coordinate = y_space
        bottom_text_y_coordinate = font.getsize(province_code)[1] + y_space*2

        # calculate coordinate for top text
        province_code_x_size = font.getsize(province_code)[0] - character_offset
        area_code_x_size = font.getsize(area_code)[0] - character_offset

        top_text_total_x_size = province_code_x_size + inline_space_size + dashline_size[0] + inline_space_size + area_code_x_size
        top_text_starting_x_coor = int((plate_width - top_text_total_x_size)/2)

        province_code_0_coor = (top_text_starting_x_coor, top_text_y_coordinate)
        province_code_1_coor = (province_code_0_coor[0] + font.getsize(province_code[0])[0] - character_offset, top_text_y_coordinate)
        dash_coor = (province_code_1_coor[0] + font.getsize(province_code[1])[0] + inline_space_size, top_text_y_coordinate + int(font.getsize(province_code[0])[1]/2))
        area_code_0_coor = (dash_coor[0] +  dashline_size[0] + inline_space_size, top_text_y_coordinate)
        area_code_1_coor = (area_code_0_coor[0] + font.getsize(area_code[0])[0] - character_offset, top_text_y_coordinate)

        # calculate coordinate for bottom text
        # bottom_text_total_x_size = font.getsize(number_code)[0] + dot_space_size*2 + dot_character_size[0]
        # bottom_text_starting_x_coor = int((plate_width - bottom_text_total_x_size)/2)

        # first_3_numbers_coor = (bottom_text_starting_x_coor, bottom_text_y_coordinate)
        # dot_coor = (first_3_numbers_coor[0] + font.getsize(number_code[:3])[0] + dot_space_size, bottom_text_y_coordinate + font.getsize(number_code[:3])[1] - dot_character_size[1])
        # last_2_numbers_coor = (dot_coor[0] + dot_character_size[0] + dot_space_size, bottom_text_y_coordinate)
        plate_img = Image.new('RGB', (plate_width, plate_height), self.get_random_background_color())
        drawer = ImageDraw.Draw(plate_img)
        # start putting each top text character
        drawer.text(province_code_0_coor, province_code[0], font=font, fill=self.get_random_character_color())
        drawer.text(province_code_1_coor , province_code[1], font=font, fill=self.get_random_character_color())
        drawer.text(area_code_0_coor, area_code[0], font=font, fill=self.get_random_character_color())
        drawer.text(area_code_1_coor , area_code[1], font=font, fill=self.get_random_character_color())
        drawer.rectangle([dash_coor, (dash_coor[0]+dashline_size[0], dash_coor[1]+dashline_size[1])], fill=self.get_random_character_color())
        # draw bottom text, 3st number + dot + last 2number
        # drawer.text(first_3_numbers_coor, number_code[:3], font=font, fill=self.get_random_character_color())
        # drawer.text(last_2_numbers_coor, number_code[-2:], font=font, fill=self.get_random_character_color())

        if len(number_code) == 4:
            bottom_text_total_x_size = font.getsize(number_code)[0]
            bottom_text_starting_x_coor = int((plate_width - bottom_text_total_x_size)/2)
            drawer.text((bottom_text_starting_x_coor, bottom_text_y_coordinate), number_code, font=font, fill=self.get_random_character_color())
        else:
            dot_space_size = (int(plate_height * Config.DOT_SPACE_SIZE_RATE))
            dot_character_size = (int(plate_height * Config.DOT_CHARACTER_SIZE_RATE[0]), int(plate_height * Config.DOT_CHARACTER_SIZE_RATE[1]))

            bottom_text_total_x_size = font.getsize(number_code)[0] + dot_space_size*2 + dot_character_size[0]
            bottom_text_starting_x_coor = int((plate_width - bottom_text_total_x_size)/2)

            first_3_numbers_coor = (bottom_text_starting_x_coor, bottom_text_y_coordinate)
            dot_coor = (first_3_numbers_coor[0] + font.getsize(number_code[:3])[0] + dot_space_size, bottom_text_y_coordinate + font.getsize(number_code[:3])[1] - dot_character_size[1])
            last_2_numbers_coor = (dot_coor[0] + dot_character_size[0] + dot_space_size, bottom_text_y_coordinate)

            drawer.text(first_3_numbers_coor, number_code[:3], font=font, fill=self.get_random_character_color())
            drawer.text(last_2_numbers_coor, number_code[-2:], font=font, fill=self.get_random_character_color())
            drawer.rectangle([dot_coor, (dot_coor[0]+dot_character_size[0], dot_coor[1]+dot_character_size[1])], fill=self.get_random_character_color())
        # draw outline code
        cv_img = np.array(plate_img)
        cv_img = cv2.rectangle(cv_img, (0, 0), (plate_width, plate_height), self.get_random_character_color(), int(plate_width/20))
        full_license_text = province_code + "-" + area_code + "_"
        if len(number_code) == 4:
            full_license_text += number_code
        else:
            full_license_text += number_code[:3] + "." + number_code[-2:]
        return cv_img, full_license_text

    def get_random_plate(self, plate_width):
        plate_img, full_license_text = self.generate_plate(plate_width)
        return plate_img, full_license_text