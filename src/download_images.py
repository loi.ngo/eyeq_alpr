from imutils import paths
import argparse
import requests
import sys
import cv2
import os
import time

# ref: https://www.pyimagesearch.com/2017/12/04/how-to-create-a-deep-learning-dataset-using-google-images/

def download(urls, output_dir):
    rows = open(urls).read().strip().split("\n")
    total = 0

    for url in rows:
        try:
            # try to download the image
            r = requests.get(url, timeout=60)
     
            # save the image to disk
            p = os.path.sep.join([output_dir, "{}.jpg".format(
                str(time.time()))])
            f = open(p, "wb")
            f.write(r.content)
            f.close()
     
            # update the counter
            print("[INFO] downloaded: {}".format(p))
            total += 1
     
        # handle if any exceptions are thrown during the download process
        except:
            print("[INFO] error downloading {}...skipping".format(p))


def recheck_downloaded_image(output_dir):
    for imagePath in paths.list_images(output_dir):
    # initialize if the image should be deleted or not
        delete = False
     
        # try to load the image
        try:
            image = cv2.imread(imagePath)
     
            # if the image is `None` then we could not properly load it
            # from disk, so delete it
            if image is None:
                delete = True
     
        # if OpenCV cannot load the image then the image is likely
        # corrupt so we should delete it
        except:
            print("Except")
            delete = True
     
        # check to see if the image should be deleted
        if delete:
            print("[INFO] deleting {}".format(imagePath))
            os.remove(imagePath)


def main(args):
    download(args.urls, args.output)
    recheck_downloaded_image(args.output)


def parse_arguments(argv):
    parser = argparse.ArgumentParser()
    parser.add_argument("-u", "--urls", required=True, help="path to file containing image URLs")
    parser.add_argument("-o", "--output", required=True, help="path to output directory of images")
    return parser.parse_args()


if __name__ == '__main__':
    main(parse_arguments(sys.argv[1:]))