import os
import sys
import glob
import time
import argparse
import pickle
import cv2
from utils import load_csv_groundtruth
from plate_detector import PlateDetector


def get_img_path(prefix, input_path):
    if os.path.isdir(input_path):
        img_paths = glob.glob(os.path.join(prefix, input_path, '*.jpg'))
    else:
        path_bboxs = load_csv_groundtruth(input_path)
        img_paths = [os.path.join(prefix, e[0]) for e in path_bboxs]
    return img_paths


def main(args):
    detector = PlateDetector(args.frozen_graph_filename)
    # load validate data
    # path_bbox = load_csv_groundtruth(validate_csv_file)
    # img = cv2.imread(img_path)
    # expanded_img = np.expand_dims([img, img], axis=0)
    # with tf.Session(graph=graph) as sess:
    #     boxes, scores = sess.run([detection_boxes, detection_scores], feed_dict={image_tensor: expanded_img})
    # boxes = boxes.squeeze()
    # scores = scores.squeeze()
    # print(boxes.shape, scores.shape)
    # valid_boxes = boxes[scores>0.8, :]
    # for box in valid_boxes:
    #     img = draw_rectangle(img, box)
    # cv2.imshow('', img)
    # if cv2.waitKey() == ord('q'):
    img_paths = get_img_path(args.img_prefix_path, args.input)

    predicted_dict = {}
    for i, img_path in enumerate(img_paths):
        start = time.time()
        img = cv2.imread(img_path)
        bboxes = detector.detect_plates(img)
        bboxes = bboxes.squeeze(axis=0)
        img_name = os.path.basename(img_path)
        for bbox in bboxes:
            if img_name not in predicted_dict:
                predicted_dict[img_name] = []
            predicted_dict[img_name].append(bbox)
        print('image: {}/{}, processing time: {}'.format(i, len(img_paths), time.time() - start))
    pickle.dump(predicted_dict, open(args.pickle_save_name, 'wb'))


def parse_arguments(argv):
    parser = argparse.ArgumentParser('')
    parser.add_argument('-fpf', '--frozen_graph_filename', default='/mnt/data/loingo.data/alpr/training_plates_100000/eyeq_ssd_mobilenet_v1_coco/frozen_inference_graph.pb')
    parser.add_argument('-ipp', '--img_prefix_path', default='')
    parser.add_argument('-i', '--input', help='can be a images folder or a training formmated csv file',
                        default='/mnt/data/loingo.data/alpr/training_plates_100000/config_data/plates_100000_validate.csv_full')
    parser.add_argument('-psn', '--pickle_save_name')
    return parser.parse_args()


main(parse_arguments(sys.argv[1:]))